<?php
/*
## Script Overview:
NoGit Webhook Agent
Description : A simple script to download .zip repository file from Github/BitBucket/GitLab. This script intended to be used as webhook agent script, as solution for servera without git installed, commonly found on shared web hosting servers.

Algorithm Overview:
1. basic configuration: service type (bitbucket/github/gitlab), repo & branch to watch
2. payload handler
3. actual downloader which:
	a - download & extract zip to a random temp folder
	b - read the new .gitignore
	c - delete all files except the ignored ones
	d - moves all files except the ignored ones
	e - delete the temp folder

NOTES : Right now I still doing reckless gitignore scanning, which means I assume that the gitignore file will only contain list of files/folders need to be ignored, so any complicated configuration might introduce some error
*/

//1. The Configs
$config = Array(
	'service_type' => 1, // service type: 1 for BitBucket, 2 for GitHub, 3 for GitLab
	'user_name' => '',
	'repo_name' => '',
	'branch_name' => '',
	'account_user' => '',
	'account_pass' => '',
);

function payload_handler($user_name, $repo_name, $branch_name, $raw_post) {
    if (!(($repo_name == $config['repo_name']) && ($user_name == $config['user_name']) && ($branch_name == $config['branch_name']))) {
		exit();
	}
}

function event_before_download() {
    
}

function event_after_download() {
    
}

//2. Payload Handler

switch ($config['service_type']) {
	case 1: //BitBucket
		payload_handler(
		    $payload_param['actor']['username'], 
		    $payload_param['repository']['name'], 
		    payload_param['push']['changes'][0]['old']['name'], 
		    json_decode($HTTP_RAW_POST_DATA, TRUE) //BitBucket sends text/plain file encoding which is not handled by PHP, thus not acessible via $_POST, buat we still could access via $HTTP_RAW_POST_DATA
		);
		
		//$debugcon = print_r($payload_param, TRUE)."\n\n\n\n".$HTTP_RAW_POST_DATA;
		//error_log("\nrepo_name = ".$payload_param['repository']['name']."\nusername = ".$payload_param['actor']['username']."\nbranch_name = ".$payload_param['push']['changes'][0]['old']['name']);
		//file_put_contents("debugcon.txt", $debugcon);
		//exit();
		break;
}


//3. Downloader Routines

event_before_download();

//3.a - download & extract zip to a random temp folder
// credits : http://stackoverflow.com/questions/2140419/how-do-i-make-a-request-using-http-basic-authentication-with-php-curl?answertab=votes#tab-top

//source
$source ='';
switch ($config['service_type']) {
	case 1: //BitBucket
		$source = "https://bitbucket.org/".$config['user_name']."/".$config['repo_name']."/get/".$config['branch_name'].".zip";
		break;
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $source);
curl_setopt($ch, CURLOPT_USERPWD, $config['account_user'] . ":" . $config['account_pass']); //credit: http://stackoverflow.com/questions/2140419/how-do-i-make-a-request-using-http-basic-authentication-with-php-curl?answertab=votes#tab-top
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSLVERSION,CURL_SSLVERSION_TLSv1);
$data = curl_exec ($ch);
$error = curl_error($ch); print_r($error);
curl_close ($ch);

//put downloaded file on random folder
$temp_dir_name = md5(rand().time());
mkdir($temp_dir_name, 0777);
$destination = "./".$temp_dir_name."/temp.zip";
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);

//extract the zip
$temp_extracted_dir_path = pathinfo(realpath($destination), PATHINFO_DIRNAME);
$zip = new ZipArchive;
$res = $zip->open($destination);
if ($res === TRUE) {
	$zip->extractTo($temp_extracted_dir_path); 
	$zip->close(); 
}
//because the extracted files is contained in one big folder, we need to go deeper
$temp_dir_tree = scandir($temp_extracted_dir_path);
$temp_extracted_dir_path .= "/".$temp_dir_tree[2];
print_r($temp_extracted_dir_path);

//3.b - read the new .gitignore
//credit : http://www.w3schools.com/php/php_file_open.asp
$ignored = Array();
$gitignore_file = fopen($temp_extracted_dir_path."/.gitignore", "r") or die('failed to open gitignore file');
// Output one line until end-of-file
while(!feof($gitignore_file)) {
	$file_name = fgets($gitignore_file);
	//ignore blank lines and comments
	if ( ($file_name[0] != '#') && ($file_name[0] != ' ') ) { 
		$ignored["./".$file_name] = TRUE; 
	}
}
fclose($gitignore_file);

//also ignore the temporary files && the script itself!
$ignored["./".$temp_dir_name] = TRUE;
$ignored["./".basename(__FILE__)] = TRUE;
print_r($ignored);
//3.c - delete all files except the ignored ones

function clean_dir ($parent_path, $ignored) {
	$dir_tree = scandir($parent_path);

	foreach ($dir_tree as $key => $value) {
		$temp_path = $parent_path."/".$value;
		if ($value != '.' && $value != '..' 
			&& !(isset($ignored[$temp_path]) && $ignored[$temp_path]) 
			&& !(isset($ignored[$temp_path."/*"]) && $ignored[$temp_path."/*"])) {
			
			if (is_dir($temp_path)) {
				clean_dir($temp_path, $ignored);
				rmdir($temp_path);
			} else {
				unlink($temp_path);
			}
		}
	}
}

clean_dir(".",$ignored);

//3.d - moves all files except the ignored ones

function move_dir ($src_root, $dest_root, $parent_path, $ignored) {
	$dir_tree = scandir($src_root."/".$parent_path);

	foreach ($dir_tree as $key => $value) {
		$temp_path = $parent_path."/".$value;

		if ($value != '.' && $value != '..' 
			&& !(isset($ignored[$temp_path]) && $ignored[$temp_path]) 
			&& !(isset($ignored[$temp_path."/*"]) && $ignored[$temp_path."/*"])) {
			
			if (is_dir($temp_path)) {
				move_dir($src_root, $dest_root, $temp_path, $ignored);
			} else {
				rename($src_root."/".$temp_path, $dest_root."/".$temp_path);
			}
		}
	}
}

move_dir($temp_extracted_dir_path, ".", "", $ignored);

//3.e - delete the temp folder
clean_dir("./".$temp_dir_name, array());
rmdir("./".$temp_dir_name);


event_after_download();
